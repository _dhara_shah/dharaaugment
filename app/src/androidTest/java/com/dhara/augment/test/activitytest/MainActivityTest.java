package com.dhara.augment.test.activitytest;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.dhara.augment.test.R;
import com.dhara.augment.test.activities.MainActivity;
import com.dhara.augment.test.utils.network.RestClient;
import com.dhara.augment.test.utils.network.RestClientTestHelper;

import com.squareup.okhttp.mockwebserver.MockWebServer;
import com.squareup.okhttp.mockwebserver.MockResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Dhara Shah on 25-03-2016.
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    private MockWebServer mockWebServer;

    /**
     * A JUnit rule to launch the main activity under test </br>
     * ActivityTestRule will create and launch of the activity for you and also expose
     * the activity under test.
     */
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class, false, false);

    @Before
    public void setUp() throws Exception{
        // mock the actual service call and overwrite the base url
        mockWebServer = new MockWebServer();
        mockWebServer.start();
        RestClient.BASE_URL = mockWebServer.url("/").toString();
    }

    @Test
    public void testCanLoadMoreData() throws IOException, InterruptedException {
        initiateMockServerCalls();

        // we scroll to the end of the list
        // given the number of items in the adapter is 9
        onView(withId(R.id.recylerView)).perform(RecyclerViewActions.scrollToPosition(9));

        // we wait for some time till the service call is made
        Thread.sleep(5000);
    }

    @Test
    public void testOnClickItem() throws IOException, InterruptedException {
        initiateMockServerCalls();

        // we wait some time before a click is performed, added this for the test on the emulator
        Thread.sleep(3000);

        // given the first item is clicked on
        onView(withId(R.id.recylerView)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, ViewActions.click()));

        // the detail screen is displayed here, so we test if the full repo name is shown correctly or not
        onView(withId(R.id.txtRepoFullName))
                .check(matches(withText(("collectiveidea/clear_empty_attributes"))));

        // we also check if the toolbar is showing the name of the repo
        // given the first item is tapped on
        onView(withId(R.id.txtHeader))
                .check(matches(withText(("clear_empty_attributes"))));

        InstrumentationRegistry.getInstrumentation().waitForIdleSync();
    }

    @Test
    public void testLoadMoreAndItemClick() throws IOException, InterruptedException {
        initiateMockServerCalls();

        // wait until the records are shown, added this for the test in the emulator
        Thread.sleep(3000);

        // given the first item is clicked on
        onView(withId(R.id.recylerView)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, ViewActions.click()));

        // the detail screen is displayed here, so we test if the full repo name is shown correctly or not
        onView(withId(R.id.txtRepoFullName))
                .check(matches(withText(("collectiveidea/clear_empty_attributes"))));

        // press back and then perform load more
        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(ViewActions.click());

        // we scroll to the end of the list
        // given the number of items in the adapter is 9
        onView(withId(R.id.recylerView)).perform(RecyclerViewActions.scrollToPosition(9));

        // espresso does not require thread.sleep method, but i could get this to wait for an async task to be completed
        // to prevent dynamic_resource_idle error
        Thread.sleep(5000);
    }

    @After
    public void tearDown() throws Exception {
        mockWebServer.shutdown();
    }

    private void initiateMockServerCalls() throws IOException {
        // given that the next link is as under we pass the header
        // since=100000000000000 to get an empty list for the next pagination call
        String link = "<https://api.github.com/repositories?since=100000000000000>; rel=\"next\"," +
                "<https://api.github.com/repositories?since=0>; rel=\"previous\"";

        String fileName = "repositories.json";
        MockResponse mockResponse = new MockResponse();

        mockResponse.setHeader("Link", link);

        // set the body as the text from the json file stored in the assets folder
        // we use #InstrumentationRegistry.getContext() to be able to access the assets folder of the test application
        mockWebServer.enqueue(mockResponse
                .setResponseCode(200)
                .setBody(RestClientTestHelper.getStringFromFile(InstrumentationRegistry.getContext(), fileName)));

        // since the ActivityTestRule's third constructor is used, for the test to be executed
        // we launch the activity
        Intent intent = new Intent();
        mActivityRule.launchActivity(intent);
    }
}

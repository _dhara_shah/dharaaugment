package com.dhara.augment.test.utils.network;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Dhara Shah on 25-03-2016.
 */
public class RestClientTestHelper {
    /**
     * Converts the {@Link InputStream} to string which would then be set into mock response
     * @param is
     * @return
     * @throws IOException
     */
    public static String convertStreamToString(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    /**
     * Gets the {@Link InputStream} from the json file stored in the assets folder </br>
     * This file is used to mock the response of an api call
     * @param context
     * @param filePath
     * @return
     * @throws IOException
     */
    public static String getStringFromFile(Context context, String filePath) throws IOException {
        final InputStream stream = context.getResources().getAssets().open(filePath);

        String ret = convertStreamToString(stream);
        //Make sure you close all streams.
        stream.close();
        return ret;
    }
}

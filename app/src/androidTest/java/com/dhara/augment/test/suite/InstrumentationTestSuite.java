package com.dhara.augment.test.suite;


import com.dhara.augment.test.activitytest.MainActivityTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Runs all Junit3 and Junit4 Instrumentation tests.
 * Ref: android-testing sample on github provided by google
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({MainActivityTest.class})
public class InstrumentationTestSuite {}

package com.dhara.augment.test.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test suite that runs all tests, unit + instrumentation tests.
 * Ref: android-testing sample on github provided by google
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({InstrumentationTestSuite.class})
public class AndroidTestSuite {}

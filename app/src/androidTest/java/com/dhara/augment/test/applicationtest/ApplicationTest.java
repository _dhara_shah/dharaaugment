package com.dhara.augment.test.applicationtest;

import android.app.Application;
import android.test.ApplicationTestCase;

import com.dhara.augment.test.DharaAugmentApp;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 * Test for the application class
 */
public class ApplicationTest extends ApplicationTestCase<DharaAugmentApp> {
    private DharaAugmentApp augmentApp;

    public ApplicationTest() {
        super(DharaAugmentApp.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        createApplication();
        augmentApp = getApplication();
    }
}
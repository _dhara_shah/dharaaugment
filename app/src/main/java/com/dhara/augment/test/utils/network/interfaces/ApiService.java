package com.dhara.augment.test.utils.network.interfaces;

import com.dhara.augment.test.models.Repository;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Url;

/**
 * Created by Dhara Shah on 15-03-2016. </br>
 * Holds all the webservice calls to be made.
 */
public interface ApiService {
 @GET("repositories")
 Call<List<Repository>> getRepositories();

 @GET
 Call<List<Repository>> getRepositories(@Url String url);
}

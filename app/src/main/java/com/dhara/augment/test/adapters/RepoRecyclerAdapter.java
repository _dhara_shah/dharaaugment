package com.dhara.augment.test.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;
import com.dhara.augment.test.DharaAugmentApp;
import com.dhara.augment.test.R;
import com.dhara.augment.test.callbacks.ItemClickListener;
import com.dhara.augment.test.callbacks.LoadMoreListener;
import com.dhara.augment.test.models.RecylerViewHolder;
import com.dhara.augment.test.models.Repository;
import com.dhara.augment.test.utils.imageutilities.MyImageLoader;

/**
 * Displays the list of repositories into the recycler view </br>
 * A special ViewHolder (@link RecylerViewHolder) is required - the ViewHolder Pattern
 */
public class RepoRecyclerAdapter extends FooterLoaderAdapter<Repository>{
    private ItemClickListener mItemClickListener;
    private LoadMoreListener mLoadMoreListener;
    private RecyclerView mRecyclerView;
    private int totalItemCount;
    private int lastVisibleItem;
    private boolean isLoadMore;
    private boolean isEnd;
    private int visibleThreshold = 4;
    private ImageLoader mImageLoader;

    public RepoRecyclerAdapter(ItemClickListener itemClickListener,
                               LoadMoreListener loadMoreListener,
                               RecyclerView recyclerView) {
        mItemClickListener = itemClickListener;
        mLoadMoreListener = loadMoreListener;
        mRecyclerView = recyclerView;
        mImageLoader = MyImageLoader.getInstance(DharaAugmentApp.getAppContext()).getImageLoader();

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();

        // required to enable load more functionality
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager
                        .findLastVisibleItemPosition();
                if (!isEnd && !isLoadMore
                        && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    // The end has reached therefore load more items
                    if (mLoadMoreListener != null) {
                        mLoadMoreListener.onLoadMore();
                    }
                    isLoadMore = true;
                }
            }
        });
    }

    @Override
    public long getTheItemId(int position) {
        return mItems.get(position).getId();
    }

    @Override
    public RecyclerView.ViewHolder getTheItemViewHolder(ViewGroup viewGroup) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.individual_row, viewGroup, false);
        RecylerViewHolder holder = new RecylerViewHolder(view);
        holder.setItemClickListener(mItemClickListener);
        return holder;
    }

    @Override
    public void bindTheViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if(viewHolder instanceof RecylerViewHolder) {
            ((RecylerViewHolder)viewHolder).mTxtRepositoryName.setText(mItems.get(position).getName());
            ((RecylerViewHolder)viewHolder).mTxtDescription.setText(mItems.get(position).getDescription());
            ((RecylerViewHolder)viewHolder).mImgAvatar.setImageUrl(mItems.get(position).getOwner().getAvatarUrl(), mImageLoader);
        }
    }

    /**
     * Sets isLoadMore to false as it is not the end of the list
     */
    public void setLoaded() {
        isLoadMore = false;
    }

    /**
     * Sets isEnd to true ensuring no more data will be fetched
     */
    public void setEndHasReached() {
        isEnd = true;
        showLoading(false);
        notifyDataSetChanged();
    }
}

package com.dhara.augment.test.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dhara.augment.test.R;
import com.dhara.augment.test.models.LoaderViewHolder;

import java.util.List;

/**
 * Abstract generic Loader class that shows load more on an endless recycler view
 * @param <T>
 */
public abstract class FooterLoaderAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    protected boolean showLoader;
    private static final int VIEWTYPE_ITEM = 1;
    private static final int VIEWTYPE_LOADER = 2;

    protected List<T> mItems;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        /**
         * Returns the viewholder that has be initialized based on the row type
         */
        if(viewType == VIEWTYPE_LOADER) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_load_more, viewGroup, false);
            return new LoaderViewHolder(view);
        }else {
            return getTheItemViewHolder(viewGroup);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder recylerViewHolder, int i) {
        /**
         * If its the loader view the progress bar will be shown or made invisible
         */
        if(recylerViewHolder instanceof LoaderViewHolder) {
            LoaderViewHolder loaderViewHolder = (LoaderViewHolder)recylerViewHolder;
            if(showLoader) {
                loaderViewHolder.mLnrProgress.setVisibility(View.VISIBLE);
            }else {
                loaderViewHolder.mLnrProgress.setVisibility(View.GONE);
            }
            return;
        }

        /**
         * Binds the data view
         */
        bindTheViewHolder(recylerViewHolder, i);
    }

    @Override
    public int getItemCount() {
        if(mItems == null || mItems.size() == 0){
            return 0;
        }
        /**
         * Returns a size plus 1 for the loader view
         */
        return mItems.size() +1;
    }

    @Override
    public long getItemId(int position) {
        if(position != 0 && position == getItemCount() -1){
            /**
             * Returns -1 the id for the loader view
             */
            return -1;
        }
        return getTheItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        /**
         * The loader view has to be shown hence we return the loader view type
         */
        if(position != 0 && position == getItemCount() -1){
            return VIEWTYPE_LOADER;
        }

        /**
         * Else we return the default view which is the item view
         */
        return VIEWTYPE_ITEM;
    }

    /**
     * Status determines if the progress bar has to be shown or made invisible
     * @param status
     */
    public void showLoading(boolean status) {
        showLoader = status;
    }

    /**
     * Generic list of items that would be inflated
     * @param items
     */
    public void setItems(List<T> items) {
        mItems = items;
    }

    /**
     * Methods to be overriden in the actual Adapter class
     * @param position
     * @return
     */
    public abstract long getTheItemId(int position);
    public abstract RecyclerView.ViewHolder getTheItemViewHolder(ViewGroup viewGroup);
    public abstract void bindTheViewHolder(RecyclerView.ViewHolder viewHolder, int position);
}

package com.dhara.augment.test.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.dhara.augment.test.DharaAugmentApp;
import com.dhara.augment.test.R;
import com.dhara.augment.test.activities.DetailActivity;
import com.dhara.augment.test.adapters.RepoRecyclerAdapter;
import com.dhara.augment.test.callbacks.ItemClickListener;
import com.dhara.augment.test.callbacks.LoadMoreListener;
import com.dhara.augment.test.models.Repository;
import com.dhara.augment.test.utils.CommonUtilities;
import com.dhara.augment.test.utils.constant.Global;
import com.dhara.augment.test.utils.network.RestClient;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Dhara Shah on 15-03-2016.
 */
public class MainFragment  extends Fragment implements ItemClickListener, LoadMoreListener , Callback<List<Repository>> {
    private View mView;
    private List<Repository> mRepositoryList;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RepoRecyclerAdapter mRepoRecyclerAdapter;
    private ItemClickListener mItemClickListener;
    private LoadMoreListener mLoadMoreListener;
    private ProgressBar mProgressBar;
    private static MainFragment mFragment;
    private Callback<List<Repository>> mCallback;
    private String[] headerValues;

    public static MainFragment newInstance() {
        mFragment = new MainFragment();
        return  mFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_main, container, false);
        setHasOptionsMenu(true);
        initView();
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /**
         * Webservice call initiated </br>
         * We pass a null argument since the first time we have to load from the start
         */
        initApiCall(null);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /**
         * Refresh has been selected </br>
         * The work of this function can be modified to may be refreshing the last call made
         * but for now, it simply clears data and loads data afresh
         */
        if(item.getItemId() == R.id.action_refresh){
            mRepositoryList.clear();
            mRepoRecyclerAdapter.notifyDataSetChanged();
            initApiCall(null);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(Response<List<Repository>> response, Retrofit retrofit) {
        /**
         * if the body is null there is an error
         */
        mProgressBar.setVisibility(View.GONE);

        if(response.body() != null) {
            headerValues = response.headers().get("Link").split(";");

            /**
             *  set the adapter here or notify data set changes
             */
            mRepositoryList.addAll(response.body());
        }else {
            if(response.errorBody() != null){
                /**
                 * We display the error message to the user
                 */
                CommonUtilities.showToastMessage(response.errorBody().toString());
            }else {
                if(mRepositoryList.size() <= 0){
                    /**
                     * We display a message to the user and set the UI accordingly showing no results
                     */
                    CommonUtilities.showToastMessage(response.errorBody().toString());
                }
            }
        }
        notifyAdapter();
    }

    @Override
    public void onFailure(Throwable t) {
        mProgressBar.setVisibility(View.GONE);
        CommonUtilities.showToastMessage(t.getMessage().toString());
        notifyAdapter();
    }

    @Override
    public void onLoadMore() {
        /**
         * make a webservice call here </br>
         * we get the next url from the header </br>
         * to enable pagination
         */
        if(headerValues != null) {
            String url = headerValues[0].replace("<", "").replace(">", "");
            if (url.endsWith("{?since}")) {
                /**
                 * We are referring to the first node, so this means we do not have any next node </br>
                 * So, we notify an end has reached
                 */
                mRepoRecyclerAdapter.setEndHasReached();
            } else {
                mRepoRecyclerAdapter.showLoading(true);
                mRepoRecyclerAdapter.notifyDataSetChanged();
                initApiCall(url);
            }
        }
    }


    @Override
    public void onItemClick(int position, View view) {
        // send to the next activity to display the details of the same
        Intent intent = new Intent(DharaAugmentApp.getAppContext(), DetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Global.INTENT_REPOSITORY, mRepositoryList.get(position));
        intent.putExtra(Global.INTENT_REPOSITORY_BUNDLE, bundle);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * Initializes the views and the adapters
     */
    private void initView() {
        /**
         * Sets the listeners
         */
        mCallback = this;
        mContext = getActivity();
        mItemClickListener = this;
        mLoadMoreListener = this;

        /**
         * View initialization
         */
        mProgressBar = (ProgressBar)mView.findViewById(R.id.progressBar);
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.recylerView);

        mProgressBar.setVisibility(View.GONE);

        /**
         * Sets the recycler view and the adapter
         */
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRepositoryList = new ArrayList<>();
        mRecyclerView.setHasFixedSize(true);

        mRepoRecyclerAdapter = new RepoRecyclerAdapter(mItemClickListener,
                mLoadMoreListener, mRecyclerView);
        mRepoRecyclerAdapter.setItems(mRepositoryList);
        mRecyclerView.setAdapter(mRepoRecyclerAdapter);
    }

    /**
     * The first service call is initiated here </br>
     * Since it would be the first page we use the base url;
     * for subsequent calls we override the base url <br>
     */
    private void initApiCall(String url) {
        if(RestClient.isNetworkAvailable()) {
            Call<List<Repository>> call = null;
            if(TextUtils.isEmpty(url)) {
                mProgressBar.setVisibility(View.VISIBLE);
                call = new RestClient().getApiService().getRepositories();
            }else{
                /**
                 * Make the next call only if there are more records to load and we have internet connection
                 */
                call = new RestClient().getApiService().getRepositories(url);
            }
            call.enqueue(mCallback);
        }else {
            CommonUtilities.showToastMessage(DharaAugmentApp.getAppContext().getString(R.string.no_internet_connection));
        }
    }

    /**
     * Notifies the adapter inorder to hide the loading bar
     * and also to halt the load more functionality
     */
    private void notifyAdapter(){
        mRepoRecyclerAdapter.showLoading(false);
        mRepoRecyclerAdapter.setLoaded();
        mRepoRecyclerAdapter.notifyDataSetChanged();
    }
}

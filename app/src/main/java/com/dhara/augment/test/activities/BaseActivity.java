package com.dhara.augment.test.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dhara.augment.test.utils.CustomExceptionHandler;

/**
 * Created by Dhara Shah on 15-03-2016.
 */
public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Add exception handling
         */
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof CustomExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler(this));
        }
    }
}

package com.dhara.augment.test.models;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.dhara.augment.test.R;

/**
 * Created by USER on 16-03-2016.
 */
public class LoaderViewHolder extends RecyclerView.ViewHolder {
    public LinearLayout mLnrProgress;

    public LoaderViewHolder(View itemView) {
        super(itemView);
        mLnrProgress = (LinearLayout)itemView.findViewById(R.id.lnrProgress);
    }
}

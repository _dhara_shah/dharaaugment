package com.dhara.augment.test.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dhara Shah on 15-03-2016.
 */
public class Repository implements Parcelable{
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("private")
    private boolean isPrivate;
    @SerializedName("description")
    private String description;
    @SerializedName("url")
    private String repoURL;
    @SerializedName("full_name")
    private String repoFullName;
    @SerializedName("owner")
    private RepoOwner owner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRepoURL() {
        return repoURL;
    }

    public void setRepoURL(String repoURL) {
        this.repoURL = repoURL;
    }

    public String getRepoFullName() {
        return repoFullName;
    }

    public void setRepoFullName(String repoFullName) {
        this.repoFullName = repoFullName;
    }

    public RepoOwner getOwner() {
        return owner;
    }

    public void setOwner(RepoOwner owner) {
        this.owner = owner;
    }

    public Repository(Parcel in) {
        this.id = in.readInt();
        this.name=in.readString();

        boolean[] val = new boolean[1];
        in.readBooleanArray(val);
        this.isPrivate = val[0];

        this.description = in.readString();
        this.repoURL = in.readString();
        this.repoFullName = in.readString();
        this.owner = in.readParcelable(RepoOwner.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(this.id);
        parcel.writeString(this.name);
        parcel.writeBooleanArray(new boolean[]{this.isPrivate});
        parcel.writeString(this.description);
        parcel.writeString(this.repoURL);
        parcel.writeString(this.repoFullName);
        parcel.writeParcelable(this.owner, flags);
    }

    public static final Creator<Repository> CREATOR = new Creator() {
        @Override
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };
}


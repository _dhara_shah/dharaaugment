package com.dhara.augment.test.utils.constant;

/**
 * Created by Dhara Shah on 15-03-2016. </br>
 * Constant class that stores all the strings used throughout the application
 */
public class Global {
    public static final String INTENT_REPOSITORY = "repository";
    public static final String INTENT_REPOSITORY_BUNDLE = "repository_bundle";
    public static final String INTENT_ISSUE_HOLDER="issue_holder";
    public static final String INTENT_ISSUE_HOLDER_BUNDLE="issue_holder_bundle";
    public static final String INTENT_ISSUE ="issue";
    public static final String INTENT_POSITION_SELECTED="position_selected";
}

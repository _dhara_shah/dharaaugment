package com.dhara.augment.test.utils.imageutilities;

import android.content.Context;

import com.android.volley.toolbox.ImageLoader;
import com.dhara.augment.test.utils.CommonUtilities;
import com.dhara.augment.test.utils.network.ApiCalls;

/**
 * Created by Dhara Shah on 15-03-2016. </br>
 * Singleton for loading images using volley
 */
public class MyImageLoader {
    private static Context mCntx;
    private static MyImageLoader myImageLoader;
    private ImageLoader mImageLoaderInstance;

    public static MyImageLoader getInstance(Context cntx) {
        mCntx = cntx;
        if(myImageLoader == null) {
            myImageLoader = new MyImageLoader();
        }
        return myImageLoader;
    }

    public ImageLoader getImageLoader() {
        if(mImageLoaderInstance == null) {
            mImageLoaderInstance = new ImageLoader(ApiCalls.getInstance(mCntx).getRequestQueue(),
                    new MyLruBitmapCache(CommonUtilities.getCacheSize()));
        }
        return mImageLoaderInstance;
    }
}

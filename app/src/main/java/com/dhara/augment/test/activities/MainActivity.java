package com.dhara.augment.test.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;

import com.dhara.augment.test.R;
import com.dhara.augment.test.fragments.MainFragment;


public class MainActivity extends BaseActivity {
    private Toolbar mToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);
        initToolbar();
        initFragment();
    }

    private void initFragment(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_frame, MainFragment.newInstance());
        ft.commit();
    }

    private void initToolbar(){
        mToolbar  = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }
}

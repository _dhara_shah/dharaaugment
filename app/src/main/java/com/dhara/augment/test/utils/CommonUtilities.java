package com.dhara.augment.test.utils;

import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.dhara.augment.test.DharaAugmentApp;
import com.dhara.augment.test.R;
import com.dhara.augment.test.customviews.CustomTextView;

/**
 * Created by Dhara Shah on 15-03-2016.</br>
 * Holds all the actions commonly needed.
 */
public class CommonUtilities {
    /**
     * Returns a cache size equal to approximately three screens worth of images.
     */
    public static int getCacheSize() {
        final DisplayMetrics displayMetrics = DharaAugmentApp.getAppContext().getResources().
                getDisplayMetrics();
        final int screenWidth = displayMetrics.widthPixels;
        final int screenHeight = displayMetrics.heightPixels;
        // 4 bytes per pixel
        final int screenBytes = screenWidth * screenHeight * 4;

        return screenBytes * 3;
    }

    /**
     * Displays a custom toast
     * @param message
     */
    public static void showToastMessage(String message) {
        Toast toast = new Toast(DharaAugmentApp.getAppContext());
        View view = LayoutInflater.from(DharaAugmentApp.getAppContext()).inflate(R.layout.layout_toast,null);
        toast.setView(view);
        CustomTextView txtMessage= (CustomTextView)view.findViewById(R.id.txtMessage);
        txtMessage.setText(message);
        toast.setGravity(Gravity.BOTTOM,0,120);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }
}

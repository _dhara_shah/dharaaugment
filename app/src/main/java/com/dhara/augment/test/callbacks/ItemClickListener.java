package com.dhara.augment.test.callbacks;

import android.view.View;

/**
 * Created by Dhara Shah on 15-03-2016. </br>
 * Listens to the item clicks on the recycler view </br>
 * Implemented by {@link com.dhara.augment.test.fragments.MainFragment}
 */
public interface ItemClickListener {
    /**
     * Gets the position of the item clicked on
     * @param position
     * @param view
     */
    void onItemClick(int position, View view);
}

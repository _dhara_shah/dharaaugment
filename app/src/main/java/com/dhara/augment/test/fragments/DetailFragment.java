package com.dhara.augment.test.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.dhara.augment.test.DharaAugmentApp;
import com.dhara.augment.test.R;
import com.dhara.augment.test.customviews.CustomTextView;
import com.dhara.augment.test.models.Repository;
import com.dhara.augment.test.utils.constant.Global;
import com.dhara.augment.test.utils.imageutilities.MyImageLoader;

/**
 * Created by Dhara Shah on 15-03-2016.
 */
public class DetailFragment extends Fragment {
    private static DetailFragment mFragment;
    private CustomTextView mTxtRepoFullName;
    private CustomTextView mTxtRepositoryName;
    private CustomTextView mTxtDescription;
    private CustomTextView mTxtOwnerName;
    private NetworkImageView mImgOwner;
    private CustomTextView mTxtIsRepoPrivate;
    private CustomTextView mTxtRepoURL;
    private ImageLoader mImgLoader;
    private Repository mRepository;
    private View mView;

    /**
     * Creates a new fragment and sets the params as an argument
     * @param repository
     * @return
     */
    public static DetailFragment newInstance(Repository repository){
        mFragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(Global.INTENT_REPOSITORY, repository);
        mFragment.setArguments(args);
        return mFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Get the arguments if any
         */
        if(getArguments()!=null){
            mRepository = getArguments().getParcelable(Global.INTENT_REPOSITORY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_details, container, false);
        initView();
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setData();
    }

    private void initView() {
        /**
         * Initialization of the imageloader
         */
        mImgLoader = MyImageLoader.getInstance(DharaAugmentApp.getAppContext()).getImageLoader();

        /**
         * Initialization of the views
         */
        mTxtRepoURL = (CustomTextView)mView.findViewById(R.id.txtRepoURL);
        mTxtIsRepoPrivate =  (CustomTextView)mView.findViewById(R.id.txtRepoPrivacy);
        mTxtRepoFullName = (CustomTextView)mView.findViewById(R.id.txtRepoFullName);
        mTxtDescription = (CustomTextView)mView.findViewById(R.id.txtDescription);
        mTxtRepositoryName =  (CustomTextView)mView.findViewById(R.id.txtRepositoryName);
        mTxtOwnerName = (CustomTextView)mView.findViewById(R.id.txtOwnerName);
        mImgOwner = (NetworkImageView)mView.findViewById(R.id.imgOwner);
    }

    /**
     * Set data into the views
     */
    private void setData(){
        if(mRepository != null) {
            mTxtDescription.setText(removeTextSpacing(mRepository.getDescription()));
            mTxtRepositoryName.setText(removeTextSpacing(mRepository.getName()));
            mTxtOwnerName.setText(removeTextSpacing(mRepository.getOwner().getLogin()));
            mTxtRepoFullName.setText(removeTextSpacing(mRepository.getRepoFullName()));
            mTxtIsRepoPrivate.setText(removeTextSpacing(""+mRepository.isPrivate()));
            mTxtRepoURL.setText(removeTextSpacing(mRepository.getRepoURL()));
            mImgOwner.setImageUrl(mRepository.getOwner().getAvatarUrl(), mImgLoader);
        }
    }

    /**
     * Function that removes extra spacing from the string
     * @param text
     * @return
     */
    private String removeTextSpacing(String text){
        return text.trim();
    }
}

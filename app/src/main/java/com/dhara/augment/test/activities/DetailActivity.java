package com.dhara.augment.test.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.dhara.augment.test.R;
import com.dhara.augment.test.customviews.CustomTextView;
import com.dhara.augment.test.fragments.DetailFragment;
import com.dhara.augment.test.models.Repository;
import com.dhara.augment.test.utils.constant.Global;

/**
 * Created by Dhara Shah on 15-03-2016.
 */
public class DetailActivity extends BaseActivity {
    private Toolbar mToolbar;
    private CustomTextView mTxtTitle;
    private Repository mRepository;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);

        if(getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getBundleExtra(Global.INTENT_REPOSITORY_BUNDLE);
            mRepository = bundle.getParcelable(Global.INTENT_REPOSITORY);
        }

        initToolbar();
        initFragment();
    }

    /**
     * Toolbar initialization
     */
    private void initToolbar(){
        mToolbar  = (Toolbar)findViewById(R.id.toolbar);
        mTxtTitle = (CustomTextView)findViewById(R.id.txtHeader);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mTxtTitle.setText(mRepository != null ? mRepository.getName() : getString(R.string.app_name));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initFragment(){
        /**
         * Fragment initialization
         */
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container_frame, DetailFragment.newInstance(mRepository));
        transaction.commit();
    }
}

package com.dhara.augment.test;

import android.app.Application;
import android.content.Context;

/**
 * Created by Dhara Shah on 15-03-2016.
 */
public class DharaAugmentApp extends Application {
    private static DharaAugmentApp mApp;
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
        mContext = this;
    }

    /**
     * returns the app context
     * @return
     */
    public static DharaAugmentApp getAppContext(){
        if(mApp == null) {
            mApp = (DharaAugmentApp)mContext;
        }
        return mApp;
    }
}

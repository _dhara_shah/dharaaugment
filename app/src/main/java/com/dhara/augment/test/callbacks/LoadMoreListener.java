package com.dhara.augment.test.callbacks;

/**
 * Created by Dhara Shah on 15-03-2016. </br.
 * Listens for a load more event </br>
 * Implemented by {@link com.dhara.augment.test.fragments.MainFragment}
 */
public interface LoadMoreListener {
    /**
     * Triggers a load more
     */
    void onLoadMore();
}

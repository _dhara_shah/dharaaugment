package com.dhara.augment.test.models;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.android.volley.toolbox.NetworkImageView;
import com.dhara.augment.test.R;
import com.dhara.augment.test.callbacks.ItemClickListener;
import com.dhara.augment.test.customviews.CustomTextView;

/**
 * Created by Dhara Shah on 15-03-2016.
 */
public class RecylerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public CustomTextView mTxtRepositoryName;
    public CustomTextView mTxtDescription;
    public NetworkImageView mImgAvatar;
    public LinearLayout mLnrRow;
    private ItemClickListener mItemClickListener;

    public RecylerViewHolder(View itemView) {
        super(itemView);

        /**
         * A recycler view does not allow onItemClick
         * hence we implement the click listener on the views
         */
        mTxtRepositoryName = (CustomTextView)itemView.findViewById(R.id.txtRepositoryName);
        mTxtDescription = (CustomTextView)itemView.findViewById(R.id.txtDescription);
        mImgAvatar = (NetworkImageView)itemView.findViewById(R.id.imgAvatar);
        mLnrRow = (LinearLayout)itemView.findViewById(R.id.lnrRow);
        mLnrRow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        mItemClickListener.onItemClick(getLayoutPosition(), view);
    }

    /**
     * Sets the item click listener so that the callback can be made to the fragment
     * Used by {@link}
     * @param listener
     */
    public void setItemClickListener(ItemClickListener listener) {
        mItemClickListener = listener;
    }
}

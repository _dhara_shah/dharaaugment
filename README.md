# About **Dhara Augment** (the app) #
- This application is a small demo that gets a list of repositories using the github api.

# Features #
- View the list of repositories
- View the details of the repository
- Load more functionality to load more repositories
- Refresh and reload the repositories from the start by tapping on "Refresh" under the options menu

# How to setup #
- Install Android studio of any version and clone this repository
- Build and make this project just like every other android studio project
- No extra library modules are needed
- Those needed are handled by gradle

# Third party libraries used #
- Appcompat v7:23.1.1
- OkHTTP used by retrofit
- Retrofit v2.0 beta version for making url calls
- GSON for conversions of json to POJO classes
- Recycler view to display the data
- CardView
- Volley for lazy loading of images over the network

# Testing #
- This app has been tested on devices running Android Lolipop and also Android marshmallow
- Suitable for all handsets except tablets

# Test cases #
The following test cases have been included:

- Load json from a file instead of the server
- Click the first item
- Compare if the first item is the same as the first element of the json object
- On load more , the next set of data would be an empty array

Junit4 has been used and the test can be executed on both devices and emulators just that ensure the following:

1. When running the appTest module, go to edit configuration and select Test: class, and select the AndroidTestSuite class or MainTestActivity class
2. Do the same for android tests under the default category

# Things not to be used (probably) for Android Testing #

I have used Thread.sleep to prevent onView actions from taking place before the records are even displayed on the screen, and Espresso is meant to overcome the usage of Thread.sleep calls, but since, I was getting an error DYNAMIC_RESOURCE_IDLE error, I used the Thread.sleep methods.

# Test framework #
Espresso and AndroidJunitRunner have been used, together with MockWebServer by square under OkHttp to mock server calls.

# Information #
- The debug apk is in the same folder as the application's main folder
- If you build this application in the release mode, a signed apk will be generated, ready to be uploaded on the PlayStore
- The signed release apk will be found in the location app/build/output/apk/ with the name "DharaAugment-1.0v.apk"
- The password to be entered is "android" (without the quotes) for all the cases and the alias is "augment" (without the quotes)

# Limitations #
- Extra features can be added to this like showing the list of contributors or issues that have been filed lately in the detail section
- Can have a list of followers and also those who have starred the repository
- The refresh functionality currently refreshes all the data, and fetches data from the start,
so instead of that we could have just the last call being retried

# Contact #
- You can contact **sdhara2@hotmail.com** incase of any doubts or queries